package models

type User struct {
	Id		 int64  `json:"id" gorm:"primaryKey"`
	Name 	 string	`json:"name" binding:"required"`
	Email    string `json:"email" binding:"required"`
    Password string `json:"password" binding:"required"`
}